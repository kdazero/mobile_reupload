using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopControl : MonoBehaviour
{
    /************************************
    ***************商店UI****************
    ************************************/
    public GameObject ShopUI;
    //public GameObject shop_text;
    public static bool ShopOpen=false;
    // Update is called once per frame
    void Update()
    {
        if (ShopOpen == false)
        {
            if (player.InShop)
            {
                
                if (player.shop_button_click)
                {
                    ShopUI.SetActive(true);
                    Debug.Log("Open");
                    ShopOpen = true;
                }
            }
        }
    }
}
