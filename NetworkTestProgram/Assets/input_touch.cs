using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class input_touch : MonoBehaviour
{
    public InputField inputfield;
    TouchScreenKeyboard keyboard;
    public void OnClick_rawImage_showLeaveWordContents()
    {
        Debug.Log("show keyboard");
        
        TouchScreenKeyboard.Open("", TouchScreenKeyboardType.Default, false, false, true);
    }
    public void end_edit(){
        inputfield.touchScreenKeyboard.active = false;
    }
    /*void OnGUI(){
        if (GUI.Button(new Rect(0, 10, 200, 32), "Open keyboard"))
        keyboard = TouchScreenKeyboard.Open("text");
    }*/
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
         if (keyboard != null)
        {
            if (Input.deviceOrientation == DeviceOrientation.FaceDown)
                keyboard.active = false;
            if (Input.deviceOrientation == DeviceOrientation.FaceUp)
                keyboard.active = true;
        }
    }
}
