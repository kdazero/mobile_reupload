using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundSensor : MonoBehaviour
{
    /************************************
    ***********判斷是否碰到地板***********
    ************************************/
    public static bool isGrounded;
    Rigidbody rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        RaycastHit hit; Physics.Raycast(transform.position, Vector3.down,out hit, 0.4f);
        if (hit.collider != null)
        { 
            isGrounded = true; 
        }
        else
        { 
            isGrounded = false;
        }
        Debug.Log(isGrounded);  
    }
}
