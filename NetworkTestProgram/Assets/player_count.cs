using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class player_count : MonoBehaviour
{   
    /************************************
    ************房內玩家人數*************
    ************************************/
    public Text room_player;
    public static int player_in_game = -1;
    public GameObject start_area;
    public static bool ReadyAreaOpen;
    
        // 計算房間內目前玩家總數
    void Start()
    {
        start_area.SetActive(false);
    }
    void Update()
    {
        player_in_game = GameObject.FindGameObjectsWithTag("Player").Length;
        room_player.text = "目前玩家人數" + player_in_game;
        if(player_in_game == 4 && Lobby.IsStart == false)
        {
            start_area.SetActive(true);
            ReadyAreaOpen = true;
        }
        if(player_in_game <= 3 || Lobby.begin_second == 0)
        {
            ReadyAreaOpen = false;
            start_area.SetActive(false);
        }
    }
}
