using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class CreateAndJoin : MonoBehaviourPunCallbacks
{
    /************************************
    ************創建及加入房間************
    ************************************/
    [Tooltip("遊戲室玩家人數上限為4人，該房已滿，請開新房")]
    [SerializeField]private byte maxplayer = 4;
   public InputField CreateInput;
   public InputField JoinInput;
    public GameObject BTclickSound;
   void Start(){
       //CreateInput.touchScreenKeyboard.active = true;
   }
   public void CreateRoom()
   {
        Instantiate(BTclickSound);
       PhotonNetwork.CreateRoom(CreateInput.text,new Photon.Realtime.RoomOptions{ MaxPlayers = maxplayer});
   }
   public void JoinRoom(){
        Instantiate(BTclickSound);
       PhotonNetwork.JoinRoom(JoinInput.text);
   }
   public override void OnJoinedRoom(){
       PhotonNetwork.LoadLevel("Game");
   }
   public override void OnJoinRandomFailed(short returnCode, string message)
    {
        
    }
}
