using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Cinemachine;
public class player : MonoBehaviourPun
{
    /************************************
    ***********角色相關程式************
    ************************************/
    PhotonView view;
    Collider col;
    Animator animator;
    public CinemachineFreeLook vcam;
    public CinemachineBrain CamaraBrain;
    // 商店用速度
    public static float speed = 10;
    // 角色移動速度
    public float walkspeed = 10;
    public float ySpeed;
    // 商店用跳躍
    public static float JumpSpeed = 6;
    // 角色跳躍速度
    public float playerJumpspeed = 10; 
    // 搖桿
    public Joystick variableJoystick;
    public Joystick CamaraJoystick;
    public Rigidbody rb;
    public GameObject player_canvas;
    public GameObject shop_button;
    public Text num_text;
    public GameObject WalkSourse;
    public GameObject JumpSourse;
    public GameObject BuySourse;
    public GameObject BTckickSound;
    public FixedTouchField TouchField;

    private Vector3 TouchPos;
    float deltaX;
    float deltaY;
    //int num = 0;
    bool isJump = false;
    int death_area_trigger_time = 0;
    public static bool player_dead = false;
    public static bool InShop = false;
    public static bool shop_button_click = false;
    public static int coin = 0;
    Vector3 FingerInput;
    bool BigJump;
    bool out_of_y = false;
    public GameObject BigJumpBT;
    public static bool BuyBigJump;
    public CinemachineBrain CinemachineBrain;
    public Text ShopTitle;
    public Text ReadyTitle;
    // Start is called before the first frame update
    private bool isGround()
    {
        return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x, col.bounds.min.y, col.bounds.center.z), 0.05f,1<<6);
    }
    void Start()
    {
        ShopTitle.rectTransform.position = CinemachineBrain.OutputCamera.WorldToScreenPoint(GameObject.Find("ShopPoint").transform.position);
        shop_button.SetActive(false);
        rb = GetComponent<Rigidbody>();
        view = GetComponent<PhotonView>();
        col= GetComponent<CapsuleCollider>();
        animator= GetComponent<Animator>();
    }
    void FixedUpdate()
    {
        //Debug.Log("reset : " + death.all_dead);
        if(view.IsMine)
        {
            ////Debug.Log(CinemachineBrain.OutputCamera.WorldToScreenPoint(GameObject.Find("ShopPoint").transform.position));
            if(Lobby.IsStart)
            {
                ShopTitle.gameObject.SetActive(false);
                ReadyTitle.gameObject.SetActive(false);
            }
            else
            {
                ShopTitle.gameObject.SetActive(true);
                ReadyTitle.gameObject.SetActive(true);
            }
            try
            {
                if (CinemachineBrain.OutputCamera.WorldToScreenPoint(GameObject.Find("ShopPoint").transform.position).z < 0)
                {
                    ShopTitle.enabled = false;
                }
                else
                {
                    ShopTitle.enabled = true;
                    ShopTitle.rectTransform.position = CinemachineBrain.OutputCamera.WorldToScreenPoint(GameObject.Find("ShopPoint").transform.position);
                }
            }
            catch
            { }
            try
            {
                if (player_count.ReadyAreaOpen && Lobby.IsStart == false)
                {
                    ReadyTitle.gameObject.SetActive(true);
                    if (CinemachineBrain.OutputCamera.WorldToScreenPoint(GameObject.Find("ReadyPoint").transform.position).z < 0)
                    {
                        ReadyTitle.enabled = false;
                    }
                    else
                    {
                        ReadyTitle.enabled = true;
                        ReadyTitle.rectTransform.position = CinemachineBrain.OutputCamera.WorldToScreenPoint(GameObject.Find("ReadyPoint").transform.position);
                    }
                }
                else
                {
                    ReadyTitle.gameObject.SetActive(false);
                }
            }
            catch
            { }
           // //Debug.Log("player_y: "+this.transform.position.y);
            // 購買後改變 或 不變
            walkspeed = speed;
            playerJumpspeed = JumpSpeed;

            // 模擬重力
            ySpeed += Physics.gravity.y * Time.deltaTime;
                
            if (isGround())
            {
                animator.SetBool("Jump", false);
                ySpeed = -0.5f;
                if (isJump || Input.GetKey(KeyCode.Space)) 
                {
                    Instantiate(JumpSourse);
                    animator.SetBool("Jump", true);
                    ySpeed = playerJumpspeed;
                    isJump = false;
                    ////Debug.Log("end jump ，now isJump bool status : " + isJump);
                }
            }
            isJump = false;
            if (BigJump)
            {
                animator.SetBool("Jump", true);
                ySpeed = 20;
                BigJump = false;
                BuyBigJump = false;
            }

            // 角色移動
            Vector3 MoveComputer = new Vector3(Input.GetAxis("Horizontal") * walkspeed, ySpeed, Input.GetAxis("Vertical") * walkspeed);
            //Vector3 Move = Vector3.forward * variableJoystick.Vertical * walkspeed + Vector3.right * variableJoystick.Horizontal * walkspeed + Vector3.up * ySpeed;
/*error*/   Quaternion CameraRotation = Quaternion.Euler(0, Camera.current.transform.rotation.eulerAngles.y, 0);
            // 角色轉向攝影機面相方向
            //Vector3 Direction = CameraRotation * Move;
            Vector3 DirectionComputer = CameraRotation * MoveComputer;
            // 角色旋轉
            if ( variableJoystick.Vertical != 0 || variableJoystick.Horizontal != 0|| Input.GetAxis("Horizontal")!=0|| Input.GetAxis("Vertical")!=0)
            {
                float faceAngle = Mathf.Atan2(variableJoystick.Horizontal, variableJoystick.Vertical) * Mathf.Rad2Deg;
                Quaternion CharaterRotation = Quaternion.Euler(0, faceAngle+ Camera.current.transform.rotation.eulerAngles.y, 0);
                Quaternion Rotation = Quaternion.Lerp(rb.rotation,CharaterRotation,0.7f);
                transform.rotation = Rotation;
                animator.SetBool("Run", true);
                if (isGround())
                {
                    WalkSourse.SetActive(true);
                }
            }
            else
            {
                WalkSourse.SetActive(false);
                animator.SetBool("Run", false);
            }
            //rb.velocity = Direction;
            rb.velocity = DirectionComputer;

            if (BuyBigJump)
            {
                BigJumpBT.SetActive(true);
            }
            else
            {
                BigJumpBT.SetActive(false);
            }
            if(Input.GetKeyDown(KeyCode.Z)){
                this.transform.position = new Vector3(0,70,-40);
            }
            //用手指拖曳轉動視角
            //CinemachineCore.GetInputAxis = GetAxisCustom;
            /*
            if(death.all_dead){
            this.transform.position = new Vector3(Random.Range(-150,-50), 75, Random.Range(-90,10));
            death.all_dead = false;
            }*/
        }
        else
        {
            vcam.enabled = false;
            player_canvas.SetActive(false);
        }
        
    }
    //判斷角色觸發
    void OnTriggerEnter(Collider other)
    {
        //商店
        if(other.tag=="Shop")
        {
            //Debug.Log("In shop area");
            shop_button.SetActive(true);
            InShop = true; 
        }
        //準備區域
        if(other.tag == "start")
        {
            this.num_text.gameObject.SetActive(false);
            //Debug.Log("in ready area");
            Lobby.start_count += 1;
        }
        
        // 死亡觀看平台
        if(other.tag == "Dead")
        {
            this.transform.position = new Vector3(Random.Range(110,210), 30, Random.Range(-87,27));
        }
        if(other.tag == "DeadLook"){
            //Debug.Log("In death_look trigger");
            this.num_text.gameObject.SetActive(true);
            death.death_count += 1;
            int num = 0;
            if(death.death_count == 1){
                num = 4;
            }else if(death.death_count == 2){
                num = 3;
            }else if(death.death_count == 3){
                num = 2;
            }else if(death.death_count == 4){
                num = 1;
            }
            this.num_text.text = "本次名次" + num;
            MoneyContal.Money += (11 - num) * 10;
            JumpSpeed = 6;
            speed = 10;
            BuyBigJump = false;
        }
    }
    void OnTriggerExit(Collider other)
    {
        shop_button.SetActive(false);
        if (other.tag == "Shop")
        {
            //Debug.Log("Out shop area");
            InShop = false;
        }
        if(other.tag == "start"){
            //Debug.Log("out ready area");
            Lobby.start_count -= 1;
            if(Lobby.start_count == 4){
                Lobby.begin_second = 30;
            }
        }
    }
    // 是否觸發跳躍
    public void JumpOnClick()
    {
        isJump = true;
        ////Debug.Log("isJump , isJump bool status :"+isJump);
    }
    //大跳躍
    public void BigJumpOnClick()
    {
        Instantiate(JumpSourse);
        BigJump = true;
        BigJumpBT.SetActive(false);
        Shop.HaveBigJump = false;
    }
    // 商店按鈕
    public void Shopping()
    {
        Instantiate(BTckickSound);
        shop_button_click = true;
    }
    //抓取手指拖曳數值
    public float GetAxisCustom(string Finger)
    {
        if (Finger=="Mouse X")
        {
            return TouchField.TouchDist.x*0.05f;
        }
        if (Finger == "Mouse Y")
        {
            return -TouchField.TouchDist.y * 0.03f;
        }
        return 0;
        /*startTouchPos = Input.touches[0].position;
        if (startTouchPos.y < 350)
        {
            startTouchPos = Input.touches[1].position;
            deltaX = startTouchPos.x - Input.touches[1].position.x;
            deltaY = startTouchPos.y - Input.touches[1].position.y;
            FingerInput = new Vector3(deltaX, deltaY, 0);
        }
        else
        {
            //計算垂直、水平移動距離
            deltaX = startTouchPos.x - Input.touches[0].position.x;
            deltaY = startTouchPos.y - Input.touches[0].position.y;
            FingerInput = new Vector3(deltaX, deltaY, 0);
        }
        if (Finger == "Mouse X")
        {
            return FingerInput.x * Time.deltaTime;
        }
        if (Finger == "Mouse Y")
        {
            return FingerInput.y * Time.deltaTime;
        }
        return 0;*/
    }
}
