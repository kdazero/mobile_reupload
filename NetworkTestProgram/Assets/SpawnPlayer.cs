using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Photon.Pun;
using Cinemachine;
public class SpawnPlayer : MonoBehaviourPun
{
    /************************************
    ***********角色隨機生成位置***********
    ************************************/
    public GameObject playerPrefab;
    public float minX;
    public float maxX;
    public float minZ;
    public float maxZ;
    public float maxY;
    private void Start()
    {
        //Debug.Log("in");
        Vector3 randomPosition = new Vector3(Random.Range(minX,maxX),maxY,Random.Range(minZ,maxZ));
        PhotonNetwork.Instantiate(playerPrefab.name,randomPosition,Quaternion.identity);
    }
}
