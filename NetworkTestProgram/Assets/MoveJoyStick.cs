using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoveJoyStick : MonoBehaviour
{
    public Joystick Move;
    public static bool IsMove;
    // Update is called once per frame
    void Update()
    {
        if (Move.Vertical != 0 || Move.Horizontal != 0)
        {
            IsMove = true;
        }
        else
        {
            IsMove = false;
        }
    }
}
