using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoneyContal : MonoBehaviour
{
    public static int Money=500;
    // Start is called before the first frame update
    void Awake()
    {
        Money=PlayerPrefs.GetInt("Money");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.P))
        {
            Money += 100;
        }
        PlayerPrefs.SetInt("Money", Money);
    }
    private void OnApplicationQuit()
    {
        PlayerPrefs.Save();
    }
}
