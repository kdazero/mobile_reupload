using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Lobby : MonoBehaviour
{
    /************************************
    ****************大廳*****************
    ************************************/
    
    public Text begin_text;
    //public Text  ready_text;
    //public GameObject start_area;
    public GameObject Lobby_area;
    public GameObject shop_item;
    public GameObject start_item;
    public Text touch_position;
    public GameObject ShoperMusic;
    public GameObject BattleMusic;
    public GameObject playground;
    public static int start_count = 0;
    public static int begin_second = 3;
    public static bool IsStart;
    void Start(){
        Instantiate(playground, new Vector3(0, 0f, 0f), Quaternion.Euler(0, 0, 0));
        begin_text.enabled = false;
        
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.K))
        {
            start_count ++;
        }

        if(start_count == 4)
        {
            start_count = 4;
            begin_text.enabled = true;
            ShoperMusic.SetActive(false);
            BattleMusic.SetActive(true);
            InvokeRepeating("timer", 1, 1);
            begin_text.text = "" + begin_second;
        }
    }
    void timer()
    {
        begin_second -= 1;

        begin_text.text = "距離遊戲開始剩餘" + begin_second;

        if (begin_second == 0)
        {
            if(!GameObject.FindGameObjectWithTag("PlayGround")){
                Instantiate(playground, new Vector3(0, 0f, 0f), Quaternion.Euler(0, 0, 0));
            }
            CancelInvoke("timer");
            Lobby_area.SetActive(false);
            //start_area.SetActive(false);
            shop_item.SetActive(false);
            start_item.SetActive(false);
            IsStart = true;
            //Instantiate(playground, new Vector3(0, 0f, 0f), Quaternion.Euler(0, 0, 0));
            foreach (GameObject resetplayer in GameObject.FindGameObjectsWithTag("Player"))
            {
                resetplayer.transform.position = new Vector3(Random.Range(-180, -91), 75, Random.Range(-45, 45));
            }
            start_count = 0;
            begin_second = 3;
        }

    }
    
}
