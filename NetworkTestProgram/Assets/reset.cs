using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class reset : MonoBehaviour
{
    public GameObject lobby;
    public GameObject ShopItem;
    public GameObject ReadyText;
    //public GameObject plane;
    public GameObject ShopperMusic;
    public GameObject BattleMusic;
    public GameObject playground;
    private void Start()
    {
        //Instantiate(playground, new Vector3(0, 0f, 0f), Quaternion.Euler(0, 0, 0));
    }
    void Update()
    {
        if(death.all_dead)
        {
            Lobby.IsStart = false;
            if(!GameObject.FindGameObjectWithTag("PlayGround")){
                Debug.Log("Not found...so create new plane");
                Instantiate(playground, new Vector3(0, 0f, 0f), Quaternion.Euler(0, 0, 0));
            }
            BattleMusic.SetActive(false);
            ShopperMusic.SetActive(true);
            lobby.SetActive(true);
            ShopItem.SetActive(true);
            foreach(GameObject resetplayer in GameObject.FindGameObjectsWithTag("Player"))
            {
                resetplayer.transform.position = new Vector3(Random.Range(-180, -91), 75, Random.Range(-45, 45));
            }
            death.death_count = 0;
            death.all_dead = false;
            Lobby.start_count = 0;
            
        }
    }
}
