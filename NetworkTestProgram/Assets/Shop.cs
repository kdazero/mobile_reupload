using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shop : MonoBehaviour
{
    public GameObject NotEnoughMoney;
    public GameObject Refound;
    public GameObject AlreadyHave;
    public GameObject DontHave;
    public GameObject Buy;
    public AudioSource BuySourse;
    public AudioSource RefundSourse;
    public AudioSource ErrorSourse;
    public GameObject BTclickSound;
    public Text Money;
    bool HaveJump;
    bool HaveSpeed;
    public static bool HaveBigJump;
    /************************************
    ****************商店*****************
    ************************************/
    private void Update()
    {
        Money.text = MoneyContal.Money.ToString();
    }
    public void BuyJump()
    {
        if (!HaveJump)
        {
            if (MoneyContal.Money >= 50)
            {
                Instantiate(BuySourse);
                player.JumpSpeed = 12;
                MoneyContal.Money -= 50;
                StartCoroutine(SuccesefullBuy());
                HaveJump=true;
            }
            else
            {
                Instantiate(ErrorSourse);
                StartCoroutine(NotEnough());
            }
        }
        else
        {
            Instantiate(ErrorSourse);
            StartCoroutine(YouHave());
        }
    }
    public void JumpRefund()
    {
        if (HaveJump)
        {
            Instantiate(RefundSourse);
            MoneyContal.Money += 50;
            player.JumpSpeed = 6;
            StartCoroutine(Refund());
            HaveJump = false;
        }
        else
        {
            Instantiate(ErrorSourse);
            StartCoroutine(YouDontHave());
        }
    }
    public void Buyspeed()
    {
        if (!HaveSpeed)
        {
            if (MoneyContal.Money >= 50)
            {
                Instantiate(BuySourse);
                player.speed = 15;
                MoneyContal.Money -= 50;
                StartCoroutine(SuccesefullBuy());
                HaveSpeed = true;
            }
            else
            {
                Instantiate(ErrorSourse);
                StartCoroutine(NotEnough());
            }
        }
        else
        {
            Instantiate(ErrorSourse);
            StartCoroutine(YouHave());
        }
    }
    public void SpeedRefund()
    {
        if (HaveSpeed)
        {
            Instantiate(RefundSourse);
            MoneyContal.Money += 50;
            player.speed = 10;
            StartCoroutine(Refund());
            HaveSpeed = false;
        }
        else
        {
            Instantiate(ErrorSourse);
            StartCoroutine(YouDontHave());
        }
    }
    public void BuyBigJump()
    {
        if (!HaveBigJump)
        {
            if (MoneyContal.Money >= 100)
            {
                Instantiate(BuySourse);
                player.BuyBigJump = true;
                MoneyContal.Money -= 100;
                StartCoroutine(SuccesefullBuy());
                HaveBigJump = true;
            }
            else
            {
                Instantiate(ErrorSourse);
                StartCoroutine(NotEnough());
            }
        }
        else
        {
            Instantiate(ErrorSourse);
            StartCoroutine(YouHave());
        }
    }
    public void BigJumpRefund()
    {
        if (HaveBigJump)
        {
            Instantiate(RefundSourse);
            MoneyContal.Money += 100;
            player.BuyBigJump = false;
            StartCoroutine(Refund());
            HaveBigJump = false;
        }
        else
        {
            Instantiate(ErrorSourse);
            StartCoroutine(YouDontHave());
        }
    }

    public void Close()
    {
        Instantiate(BTclickSound);
        this.gameObject.SetActive(false);
        ShopControl.ShopOpen = false;
        player.shop_button_click = false;
    }
    IEnumerator NotEnough()
    {
        NotEnoughMoney.SetActive(false);
        Refound.SetActive(false);
        AlreadyHave.SetActive(false);
        DontHave.SetActive(false);
        Buy.SetActive(false);
        NotEnoughMoney.SetActive(true);
        yield return new WaitForSeconds(1f);
        NotEnoughMoney.SetActive(false);
    }
    IEnumerator Refund()
    {
        NotEnoughMoney.SetActive(false);
        Refound.SetActive(false);
        AlreadyHave.SetActive(false);
        DontHave.SetActive(false);
        Buy.SetActive(false);
        Refound.SetActive(true);
        yield return new WaitForSeconds(1f);
        Refound.SetActive(false);
    }
    IEnumerator YouHave()
    {
        NotEnoughMoney.SetActive(false);
        Refound.SetActive(false);
        AlreadyHave.SetActive(false);
        DontHave.SetActive(false);
        Buy.SetActive(false);
        AlreadyHave.SetActive(true);
        yield return new WaitForSeconds(1f);
        AlreadyHave.SetActive(false);
    }
    IEnumerator YouDontHave()
    {
        NotEnoughMoney.SetActive(false);
        Refound.SetActive(false);
        AlreadyHave.SetActive(false);
        DontHave.SetActive(false);
        Buy.SetActive(false);
        DontHave.SetActive(true);
        yield return new WaitForSeconds(1f);
        DontHave.SetActive(false);
    }
    IEnumerator SuccesefullBuy()
    {
        NotEnoughMoney.SetActive(false);
        Refound.SetActive(false);
        AlreadyHave.SetActive(false);
        DontHave.SetActive(false);
        Buy.SetActive(false);
        Buy.SetActive(true);
        yield return new WaitForSeconds(1f);
        Buy.SetActive(false);
    }
}
