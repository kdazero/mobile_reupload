using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class plane_destory : MonoBehaviour
{
    /************************************
    ***********主遊戲方塊消失************
    ************************************/
    public GameObject plane;
    public Material green;
    public Material yellow;
    public Material red;
    public float pause_time;
    public bool isEnter = false;
    void Update(){
        if(isEnter){
            pause_time += Time.deltaTime;
            if(pause_time >= 0.5f){
            plane.GetComponent<MeshRenderer> ().material = green;
                if(pause_time >= 0.9f){
                    plane.GetComponent<MeshRenderer> ().material = yellow;
                    if(pause_time >= 1.3f){
                        plane.GetComponent<MeshRenderer> ().material = red;
                        if(pause_time >=1.7f){
                            plane.SetActive(false);
                        }
                    }
                }     
            }
        }
    }
    void OnCollisionEnter(Collision collision){
        isEnter = true;
    }
}
